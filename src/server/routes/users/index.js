import express from 'express';
import { Users } from '../../../objects';
import { Uploads, Elements } from '../../../utils';

const users = new Users();
const uploads = new Uploads();
const Router = express.Router();
const Element = new Elements();

const files = [
  {
    name: 'photo',
    maxCount: 1
  }, {
    name: 'picture',
    maxCount: 1
  }, {
    name: 'cover',
    maxCount: 1
  }
];

Router.route('/')
      .get((req, res) => {
        let where   = req.where,
            orderBy = req.orderBy,
            start   = req.start,
            end     = req.end,
            fields  = req.fields;
        
        let options;

        if(orderBy && start && end)
          options = { orderBy, start, end }

        return users.find(where, fields, options, (err, docs) => {
          if(err)
            return res.json(err);
          console.log(req.user)
          return res.json(docs);
        })
      })
      .post(uploads.fields(files), (req, res, next) => {
        let body  = req.body,
            files = req.files;

        if (
            !files && 
            'photo' in files &&
            files['photo'].length > 0 && 
            'picture' in files && 
            files['picture'].length > 0 &&
            'cover' in files &&
            files['cover'].length > 0
          )
          return next();  
        
        return users.create(body, (err, doc) => {
          if(err)
            return res.json(err);

          let id = doc._id;

          return uploads.uploads(`users/${id}`, id, files, (err, uris) => { 
            if(err)
              return res.json(err);  
  
            files = {
              photo: uris.photo[0],
              picture: uris.picture[0],
              cover: uris.cover[0]
            }
            return users.updateById(id, { $set: files }, {}, (err, doc) => {
              if(err) 
                return res.json(err);
              return res.json(doc);
            })
          })  
        })
      })
      .post((req, res) => {
        let body = req.body; 

        return users.create(body, (err, doc) => {
          if(err)
            return res.json(err);

          return res.json(doc);
        })
      })
Router.route('/search/:search')
      .get((req, res) => {
        let where   = req.where,
            orderBy = req.orderBy,
            start   = req.start,
            end     = req.end,
            fields  = req.fields;

        let options = { orderBy, start, end };

        return users.find(where, fields, options, (err, docs) => {
          if(err)
            return  res.json(err);

          return  res.json(docs);
        })

      });
Router.route('/id/:id')
      .get((req, res) => {
        let id     = req.id,
            fields = req.fields;

        if(!Element.isValid(id))
          return res.json('Invalid Id');

        return users.findById(id, fields, {}, (err, doc) => {
          if(err)
            return res.json(err);

          return res.json(doc);
        })
      })
      .put((req, res) => {

      })
      .put((req, res) => {

      })
      .delete((req, res) => {
        
      })

export default Router;