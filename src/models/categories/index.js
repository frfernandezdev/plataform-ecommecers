import { db } from '../../utils';

const Schema = db.Schema;

export default db.model('categories', new Schema({
  name: String
}));