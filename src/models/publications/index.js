import { db } from '../../utils';

const Schema = db.Schema;

export default db.model('publications', new Schema({
	name: String
}));