import { db } from '../../utils';

const Schema = db.Schema;

export default db.model('localities', new Schema({
	name: String
}));