import { db } from '../../utils';

const Schema = db.Schema;

export default db.model('views', new Schema({
	name: String
}));