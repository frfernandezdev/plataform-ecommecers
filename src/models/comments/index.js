import { db } from '../../utils';

const Schema = db.Schema;

export default db.model('comments', new Schema({
	name: String
}));