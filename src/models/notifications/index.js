import { db } from '../../utils';

const Schema = db.Schema;

export default db.model('notifications', new Schema({
	name: String
}));