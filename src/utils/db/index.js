import mongoose from 'mongoose';
import config from '../../../config';

mongoose.set ( 'useCreateIndex' , true );
mongoose.connect(config.DB, { useNewUrlParser: true }, err => {
  if(err)
    return console.log(`Error the connection problem: ${err}`)
  return console.log('Connection with Mongo');
});
  
export default mongoose;