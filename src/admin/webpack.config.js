// Dependencies
const webpack      = require('webpack'),
      path         = require('path');

// Environment
const isDevelopment = process.env.NODE_ENV !== 'production';

// Paths
const PATHS = {
  index: path.join(__dirname, 'src/index'),
  build: path.join(__dirname, './public'),
  src: path.join(__dirname, 'src')
};

const getDevtool = () => 'cheap-module-eval-source-map';
const getEntry = () => {
  const entry = [PATHS.index];

  if (isDevelopment) {
    entry.push('webpack-hot-middleware/client?reload=true');
  }

  return entry;
};
const getOutput = () => ({
  path: PATHS.build,
  publicPath: '/',
  filename: '[name].bundle.js'
});
const getPlugins = () => {
  const plugins = [
    // new ChunksPlugin({
    //   to: 'vendor',
    //   test: /node_modules/
    // })
  ];

  if (isDevelopment) {
    plugins.push(
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoEmitOnErrorsPlugin()
    );
  } else {
    plugins.push(
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          screw_ie8: true,
          warnings: false
        }
      })
    );
  }

  return plugins;
};
const getLoaders = () => ({
  rules: [
    {
      test: /\.js?$/,
      loaders: ['babel-loader'],
      include: PATHS.src
    },
    {
      test: /(\.css|\.scss|\.sass)$/,
      loaders: ['style-loader', 'css-loader', 'sass-loader']
    },
    {
      test: /\.(jpe?g|png|gif)$/,
      use: [{
        loader: 'url-loader',
      }]
    },
    {
      test: /\.(eot|svg|ttf|woff2?|otf)$/,
      use: 'file-loader'
    }
  ]
});

module.exports = {
  mode: isDevelopment ? 'development' : 'production',
  devtool: getDevtool(),
  entry: getEntry(),
  output: getOutput(),
  plugins: getPlugins(),
  module: getLoaders(),
  optimization: {
    splitChunks: {}
  },
  performance: {
    hints: false
  }
};