// Dependencies
import express from 'express';
import webpack from 'webpack';
import path from 'path';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';

// Webpack Configuration
import webpackConfig from './webpack.config';

// Express app
const app = express();

// Environment
const isDevelopment = process.env.NODE_ENV !== 'production';

// Express Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Webpack Compiler
const webpackCompiler = webpack(webpackConfig);

// Webpack Middleware
if(isDevelopment){
  app.use(webpackDevMiddleware(webpackCompiler));
  app.use(webpackHotMiddleware(webpackCompiler));
}

// Public file
app.use(express.static(path.join(__dirname, 'public')));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, './public/index.html'));
});

export default app;