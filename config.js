export default {
  // Setting for API Restful
  PORT: 3002,
  DB: 'mongodb://localhost:27017/plataform-ecommerce',
  
  // Setting for Website
  PORT_WWW: 3000,
  // Setting for Admin
  PORT_ADMIN: 3001,
  // Setting for Uploads Server
  PORT_UPLOAD: 3003,
  // Setting folder Storage
  STORAGE_DIR: 'storages',
  // Setting folder Files Temporary
  TMP_DIR: 'tmp',
  // Setting Url the server storage
  STORAGE_URL: "http://localhost:3003",


  // Authentication
    hashEncrypt: '1add12daskfk432k3mf',
    // Keys Provider Google Auth
    google: {
      // Client ID
      clientID: '850029461954-6vkoajeo6e4th4sm65fvc3177nuodbgo.apps.googleusercontent.com',
      // Client key Secret
      clientSecret: 'uXJ88QFKrTptI1NSSkXrAkdJ',
      // Callback URL  
      callbackURL: '/auth/google/redirect'
    }
}